﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NewBehaviourScript : MonoBehaviour
{

    public static int groudHeigh = 18;
    public static int groudWidth = 11;
    public static Transform[,] groud = new Transform[groudWidth, groudHeigh];


    // Start is called before the first frame update
    void Start()
    {
        CreateNew();
    }

    // Update is called once per frame
    void Update()
    {

    }

   

    public void CreateNew()
    {
        GameObject newOb = (GameObject)Instantiate(Resources.Load(NewRandom(), typeof(GameObject)), new Vector2(5.75f, 19.25f), Quaternion.identity);
    }
    public bool CheckBorders(Vector2 pos)
    {
        return ((int)pos.x >= 1 && (int)pos.x < groudWidth && (int)pos.y > 1);
    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }

    public void ConvertGroud(Objects objects ) {
        for(int y = 0; y<groudHeigh; y++)
        {
            for (int x = 0; x < groudWidth; x++)
            {
                if (groud[x,y]!= null)
                {
                    if (groud[x,y].parent == objects.transform)
                    {
                        groud[x, y] = null;

                    }
                }
            }
        }
        foreach (Transform mino in objects.transform)
        {
            Vector2 pos = Round(mino.position);
            if (pos.y < groudHeigh)
            {
                groud[(int)pos.x, (int)pos.y] = mino;
            }

        }
    }

    public Transform TransfGroud (Vector2 pos)
    {
        if (pos.y > groudHeigh - 1)
        {
            return null;
        }
        else {
            return groud[(int)pos.x, (int)pos.y];
        }
    }

    string NewRandom()
    {
        int x = Random.Range(1, 7);
        string NewName = "Prefabs/T";
        switch (x)
        {
            case 1: NewName = "Prefabs/T";
                break;
            case 2: NewName = "Prefabs/G";
                break;
            case 3:
                NewName = "Prefabs/L";
                break;
            case 4:
                NewName = "Prefabs/Plus";
                break;
            case 5:
                NewName = "Prefabs/Square";
                break;
            case 6:
                NewName = "Prefabs/Long";
                break;

        }
        return (NewName);
    }
}