﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objects : MonoBehaviour
{
    float down = 0;
    public float downSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UsersPress();
    }


    void UsersPress()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.position += new Vector3(1, 0, 0);
            if (!CheckBorders())
            {
                transform.position += new Vector3(-1, 0, 0);

            }
            else { FindObjectOfType<NewBehaviourScript>().ConvertGroud(this);
            }

        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-1, 0, 0);
            if (!CheckBorders())
            { transform.position += new Vector3(1, 0, 0); }
            else
            {
                FindObjectOfType<NewBehaviourScript>().ConvertGroud(this);
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Time.time - down >= downSpeed) 
        {
            transform.position += new Vector3(0, -1, 0);
            if (CheckBorders())
            {
                FindObjectOfType<NewBehaviourScript>().ConvertGroud(this);
            }
            else
            {
                transform.position += new Vector3(0, 1, 0);
                enabled = false;
                FindObjectOfType<NewBehaviourScript>().CreateNew();
            }

            down = Time.time;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Rotate (0, 0, 90);
            if (!CheckBorders())
            { transform.Rotate (0, 0, -90); }
            else
            {
                FindObjectOfType<NewBehaviourScript>().ConvertGroud(this);
            }
        }
    }

    bool CheckBorders ()
    {
        foreach (Transform mino in transform )
        {
            Vector2 pos = FindObjectOfType<NewBehaviourScript>().Round(mino.position);
            if (FindObjectOfType<NewBehaviourScript>().CheckBorders(pos) == false)
            {
                return false;
            }
            if (FindObjectOfType<NewBehaviourScript>().TransfGroud(pos)!= null && FindObjectOfType<NewBehaviourScript >().TransfGroud(pos).parent != transform)
            {
                return false;
            }
        }
        return true;

    }

}
